---
layout: handbook-page-toc
title: Mid-Market First Order
description: >-
  Mid-Market First Order processes for prospecting, managing, and cultivating accounts.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Mid Market Key Accounts First Order Mission Statement

The Mid-Market Key Accounts First Order team was started to create and scale the process of landing key new logos for GitLab Mid-Market Sales. We venture to do this efficiently, scalably, and repeatably while fostering the core CREDIT values of GitLab. As we grow, the goal is to take these processes throughout Commercial and Enterprise Sales.

As a First Order team, we strive to be obsessed with customer success, and aim to sell to IT professionals in a manner that is efficient, comfortable, and honest. We give first and use the resources available to us to create a buying environment that will enable a relationship to foster naturally with GitLab. 

We uphold GitLab brand integrity though our outreach, and we collaborate to ensure we have the skills necessary to execute at the highest level possible. 

We are The First Order.

## Prospecting Best Practices

## First Order Skills and Skills Development

## Working Cross-Functionally

## How First Order Measures Success

## First Order Logistics

### Rules of Engagement Between MMKAFO and MM-Territory
### MMKAFO to Named Handoff Motion
### Definition of what counts as a logo

## Frequently Asked Questions (FAQ)


